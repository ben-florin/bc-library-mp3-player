<?php

define('AUDIO_FILE_URL', 'http://flash.bc.edu/libraries/audio/');
define('PLAYLIST_FILE_URL', 'http://flash.bc.edu/libraries/playlists/');

if (is_mp3_request()) {
    header("Location: " . AUDIO_FILE_URL . $_GET['audiofile']);
} elseif (is_playlist_request()) {
    header("Location: " . PLAYLIST_FILE_URL . $_GET['playlistfile']);
} elseif (is_proxy_request()) {
    header("Content-Type:text/xml");
    $xml = PLAYLIST_FILE_URL . $_GET['playlistfile'];
    $returned_content = get_data($xml);
    echo $returned_content;
} else {
    include(__DIR__ . '/view.html');
}

function get_data($url)
{
    $ch = curl_init();
    $timeout = 5;
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
}

function is_mp3_request()
{
    return is_query_mp3_request() || is_header_mp3_request();
}

function is_query_source_request()
{
    return isset($_GET['source']) && ($_GET['source'] === '1' || strtolower($_GET['source']) == 'true');
}

function is_playlist_request()
{
    return is_query_source_request() && isset($_GET['playlistfile']);
}

function is_query_mp3_request()
{
    return is_query_source_request() && isset($_GET['audiofile']);
}

function is_header_mp3_request()
{
    return (isset($_SERVER['HTTP_ACCEPT']) && strpos($_SERVER['HTTP_ACCEPT'], 'audio/mp') !== false);
}

function is_proxy_request()
{
    return isset($_GET['proxy']) && $_GET['proxy'] === '1';
}